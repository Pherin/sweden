<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-Type" content="text/html; charset=windows-1251" />
<link href="css/jquery-ui.css" rel="stylesheet"/>
<link href="css/calendar_style.css" rel="stylesheet"/>
<link href="css/jquery.bxslider.css" rel="stylesheet" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.js"></script>


<style>

body {
    color: #ffffff;
    /*background: lightsteelblue;
    background: #C4D8F3;*/
    /*background: #E7E9FF;*/


}

    /*#main_menu_wrapper {
        margin:0 auto;
        max-width:700px;padding-top:20px;
    }

    #main_menu {

        text-align:justify;
        width:100%;
        padding:0px;
        margin:0px;
    }

    #main_menu li{
        display:inline-block;
        color: #ffffff;
        font-size: 17px;
        line-height:17px;
        font-family: Arial;
        cursor:pointer;
    }

    #last_main_menu {
        width:100%;
    }*/

#main_menu_wrapper {
    margin:0 auto;
    width:1000px;
    background: #200772;
}

#main_menu {
    margin:0 auto;
    padding: 0px;
    display: table;
}

#main_menu li{
    display:inline-block;
    color: #ffffff;
    font-size: 17px;
    line-height:17px;
    font-family: Verdana;
    cursor:pointer;
    padding-top:5px;
    padding-bottom:5px;
    padding-left:15px;
    padding-right:15px;

}

    /*
    Main colors:
    #200772; #ffffff;
    #5537B9; #7059B9; #13024A; #281956, #04399B
    Font-families:
    Times NEw Roman, Georgia, Arial
    */

.ui-widget-header {
    border: 1px solid #536d9b;
    background: #536d9b; /* Old browsers */
    /* IE9 SVG, needs conditional override of 'filter' to 'none' */
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzUzNmQ5YiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjAlIiBzdG9wLWNvbG9yPSIjNTM2ZDliIiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiM1MzZkOWIiIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjMDQzOTliIiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAxJSIgc3RvcC1jb2xvcj0iIzA0Mzk5YiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
    background: -moz-linear-gradient(top,  #536d9b 0%, #536d9b 0%, #536d9b 0%, #04399b 100%, #04399b 101%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#536d9b), color-stop(0%,#536d9b), color-stop(0%,#536d9b), color-stop(100%,#04399b), color-stop(101%,#04399b)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  #536d9b 0%,#536d9b 0%,#536d9b 0%,#04399b 100%,#04399b 101%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  #536d9b 0%,#536d9b 0%,#536d9b 0%,#04399b 100%,#04399b 101%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  #536d9b 0%,#536d9b 0%,#536d9b 0%,#04399b 100%,#04399b 101%); /* IE10+ */
    background: linear-gradient(to bottom,  #536d9b 0%,#536d9b 0%,#536d9b 0%,#04399b 100%,#04399b 101%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#536d9b', endColorstr='#04399b',GradientType=0 ); /* IE6-8 */

    color: #FFF;
    font-weight: bold;
}

.info_block {
    height:20px;
    color:#444444;
    font-family:Verdana;
    font-weight:bold;
    font-size:15px;
    line-height:20px;
    margin-left: 20px;
    margin-top:10px;
}

.bx-wrapper .bx-pager {
    display:none;
}

.description_icons {
    font-family:Arial;
    font-size:12px;
    color:#000000;
    font-weight:bold;
    font-style:italic;
    width:120px;
    padding:10px;
    height:40px;
    text-align: center;
}

.image_icons {
    width:140px;
    float:left;
}

.image_icons img{
    margin:0 auto;
}

.left_border_icons {
    width:35px;
    height:20px;
    float:left;
}
.right_border_icons {
    width:34px;
    height:20px;
    float:right;
}

.slider_icons_wrapper {
    width:490px;
    height:300px;
}

.additional_menu_text {
    color:white;
    font-style:italic;
    color:#ffffff;
    padding:10px;
    padding-left:20px;
}

#additional_menu {

    /*background: rgb(32,7,114);
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzIwMDc3MiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijk5JSIgc3RvcC1jb2xvcj0iIzIwN2NjYSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
    background: -moz-linear-gradient(top,  rgba(32,7,114,1) 0%, rgba(32,124,202,1) 99%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(32,7,114,1)), color-stop(99%,rgba(32,124,202,1)));
    background: -webkit-linear-gradient(top,  rgba(32,7,114,1) 0%,rgba(32,124,202,1) 99%);
    background: -o-linear-gradient(top,  rgba(32,7,114,1) 0%,rgba(32,124,202,1) 99%);
    background: -ms-linear-gradient(top,  rgba(32,7,114,1) 0%,rgba(32,124,202,1) 99%);
    background: linear-gradient(to bottom,  rgba(32,7,114,1) 0%,rgba(32,124,202,1) 99%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#200772', endColorstr='#207cca',GradientType=0 );
    width:340px;
    height:245px;
    border-radius:4px;
    font-family: Verdana;
    box-shadow: 0px 0px 7px #000;
    opacity:0.8;*/

    background: rgb(56, 93, 138);
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzIwMDc3MiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijk5JSIgc3RvcC1jb2xvcj0iIzIwN2NjYSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
    background: -moz-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgb(56, 93, 138)), color-stop(99%, rgb(79, 129, 189)));
    background: -webkit-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
    background: -o-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
    background: -ms-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
    background: linear-gradient(to bottom, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#200772', endColorstr='#207cca',GradientType=0 );
    width:340px;
    height:245px;
    border-radius:4px;
    font-family: Verdana;
    box-shadow: 0px 0px 7px #000;
    opacity:0.8;
}


#right_menu_list {
    background: rgb(32,7,114); /* Old browsers */
    /* IE9 SVG, needs conditional override of 'filter' to 'none' */
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzIwMDc3MiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijk5JSIgc3RvcC1jb2xvcj0iIzIwN2NjYSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
    background: -moz-linear-gradient(top,  rgba(32,7,114,1) 0%, rgba(32,124,202,1) 99%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(32,7,114,1)), color-stop(99%,rgba(32,124,202,1))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(32,7,114,1) 0%,rgba(32,124,202,1) 99%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(32,7,114,1) 0%,rgba(32,124,202,1) 99%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(32,7,114,1) 0%,rgba(32,124,202,1) 99%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(32,7,114,1) 0%,rgba(32,124,202,1) 99%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#200772', endColorstr='#207cca',GradientType=0 ); /* IE6-8 */
    width:228px;
    border-radius:4px;
    font-family: Verdana;
    box-shadow: 0px 0px 7px #000;
    margin-top:15px;
    margin-right:0px;
    opacity:0.8;
}

#tel_right {
    width:228px;
    margin-top:15px;
}

#right_menu_list ul {
    margin:0 auto;
    padding:0px;
}

#right_menu_list ul li {
    list-style-type: none;
    padding-top:5px;
    padding-bottom:5px;
    height:25px;
    line-height:25px;
    color:#ffffff;
    font-family:Verdana;
    font-size:14px;
    width:180px;
    border-bottom:1px solid #200772;
    text-align:center;
    margin: 0 auto;
    cursor:pointer;
}

.ui-datepicker-inline.ui-datepicker.ui-widget.ui-widget-content.ui-helper-clearfix.ui-corner-all {
    width:220px;
}

.ui-datepicker-calendar {
    height:159px;
}

.news_block_text {
    font-family:Verdana;
    font-size:12px;
    width:200px;
    padding-left:10px;
    padding-right:10px;
    margin-top:10px;
    color:#444444;
    text-indent:20px;
}

.news_block_text a{
    text-decoration:underline;
    color:orangered;
    font-style:italic;
}

.additional_menu_ul{
    margin:0px;
    margin-top:2px;
    padding:0px;
}

.additional_menu_ul li{
    list-style-type: none;
    padding-top:5px;
    padding-bottom:5px;
    height:35px;
    line-height:35px;
    color:#ffffff;
    font-family:Verdana;
    font-size:14px;
    border-bottom:1px solid #200772;
    text-align:center;
    margin: 0 auto;
    cursor:pointer;
}

.additional_menu_ul li:hover{
    opacity:0.7;
}

.right_menu_icons {
    float:left;
    width:30px;
    text-align:center;
}

.right_menu_text {
    float:left;
    width:170px;
    margin-left:15px;
}

.right_menu_wrapper {
    height:30px;
    clear:both;
    margin-top:10px;
    color:#000000;
    font-family:Verdana;
    font-size:14px;
    font-weight:bold;
}

.header_sliders {
    height:30px;
    line-height:30px;
    text-align:center;
    width:480px;
    font-size:16px;
    font-family:Arial;
    font-style:italic;
    color: #000000;
    font-weight: bold;
}
</style>

</head>

<body style="margin:0; padding:0px;">

<div style="width:100%; background: #200772;">

    <!--<div id="main_menu_wrapper">
        <ul id="main_menu">
            <li>��������� �����</li>
            <li>��������� �����</li>
            <li>���������� ����</li>
            <li>������ ��������</li>
        </ul>
    </div>-->
</div>
<div id=content_wrapper>
<div id="man_three_blocks" style="width:1000px; margin:0 auto;">
<div id="left_main_block" style="width:341px; height:520px;float:left; position:relative;">
    <div id="logo" style="width:341px;">
        <img src="images/vz2.png" style="width:340px;"/>
    </div>
    <div id="additional_menu" style="margin-top:10px;">
        <div id="left_additional_menu" style="width:169px; float:left;">
            <ul class="additional_menu_ul">
                <li>� ��������</li>
                <li>�������</li>
                <li style="height: 45px;line-height: 15px;">�������� ������������</li>
                <li>��������� ������</li>
                <li style="border-bottom:none;">�������</li>
            </ul>
        </div>
        <div id="right_additional_menu" style="width:169px; float:right;">
            <ul class="additional_menu_ul">
                <li>���������� ����</li>
                <li>������� � ��������</li>
                <li  style="height: 45px;line-height: 45px;">��������� �����</li>
                <li>��������</li>
                <li style="border-bottom:none;">��������</li>
            </ul>
        </div>
    </div>
</div>
<div id="center_main_block" style="width:420px; float:left;height:520px; position:relative;">
    <div style="width:358px; height:480px; position:absolute; bottom:0px;left:40px;">
        <!--<img style="margin-bottom:0px;" src="images/main.png"/>-->
    </div>
</div>
<div id="right_main_block" style="width:235px;height:520px; float:right; position:relative;">
    <div class="right_menu_wrapper" style="height:40px;margin-top:20px;">
        <div class="right_menu_icons"><img width="20px" src="images/phone2.png"/></div>
        <div class="right_menu_text">
            <div>8-981-728-83-83</div>
            <div>8-981-728-85-85</div>
        </div>
    </div>

    <div class="right_menu_wrapper">
        <div class="right_menu_icons"><img width="20px" src="images/email2.png"/></div>
        <div class="right_menu_text">
            <div><a style="color:#000000;" href="mailto:v.zakone.spb@mail.ru">v.zakone.spb@mail.ru</a></div>
        </div>
    </div>

    <div class="right_menu_wrapper">
        <div class="right_menu_icons"><img width="20px" src="images/blog2.png"/></div>
        <div class="right_menu_text">
            <div>����</div>
        </div>
    </div>

    <div class="right_menu_wrapper">
        <div class="right_menu_icons"><img height="20px" src="images/map2.png"/></div>
        <div class="right_menu_text">
            <div>�. �����-���������, <br />��. ������, �.10, <br />�.9, 3-� ���� </div>
        </div>
    </div>

    <div id="datepicker" style="box-shadow: 0px 0px 12px #A8A8A8; margin: 0 auto;display: table; position:absolute; bottom:30px; right:0px;"></div>
</div>
<div style="clear:both; height:5px; width:1000px; background: #200772;"></div>
    <style>
        .services_menu {
            /*border: 1px solid #200772;*/
            border-radius: 4px;
            width:320px;
            box-shadow: 0px 0px 7px #000;
        }

        .services_menu_phis_header {
            width:160px;
            height:50px;
            line-height:50px;
            float:left;
            text-align: center;
            font-family: Verdana;
            font-size: 12px;
            color:#200772;
            font-weight:bold;
        }

        .services_menu_phis_header.not_active {
            background: rgb(56, 93, 138);
            background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJod�IgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
            background: -moz-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgb(56, 93, 138)), color-stop(99%, rgb(79, 129, 189)));
            background: -webkit-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: -o-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: -ms-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: linear-gradient(to bottom, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#200772', endColorstr='#207cca',GradientType=0 );
            color:#ffffff;
            opacity: 0.8;
            cursor:pointer;
        }

        .services_menu_ul {
            list-style-type: none;
            padding:0px;
            display:table;
        }

        .services_menu_ul li:last-child {
            margin-bottom:20px;
        }
        .services_menu_ul li {
            clear:both;
            margin-top:15px;
            margin-left:20px;
        }

        .services_menu_ul li img {
            float:left;
            height:45px;
        }

        .services_menu_ul li div {
            vertical-align:middle;
            font-family: Verdana;
            font-size: 12px;
            padding-left:20px;
            text-align:left;
            width:200px;
            color:#200772;
            font-weight:bold;
            display:table-cell;
            height:45px;
        }
    </style>

<script type="text/javascript">

   $(document).ready(function(){
       $('#phis_header').click(function(){
           if($(this).hasClass('not_active')) {
               $(this).removeClass('not_active');
               $('#jur_header').addClass('not_active');
               $('#jur_ul').fadeOut(100);
               setTimeout(function() {$('#phis_ul').fadeIn(100);}, 100);

           }
           return false;
       });

       $('#jur_header').click(function(){
           if($(this).hasClass('not_active')) {
               $(this).removeClass('not_active');
               $('#phis_header').addClass('not_active');
               $('#phis_ul').fadeOut(100);
               setTimeout(function() {$('#jur_ul').fadeIn(100);}, 100);

           }
           return false;
       });

       $('#services_menu_jur_1n').click(function(){
           $('.service_wrapper').fadeOut(100);
           setTimeout(function() {$('#bankrot_text').fadeIn(100);}, 100);
           return false;
       });
       $('#services_menu_jur_2n').click(function(){
           $('.service_wrapper').fadeOut(100);
           setTimeout(function() {$('#nedv_text').fadeIn(100);}, 100);
           return false;
       })
   })

</script>

<div style="clear:both; width:1000px; padding-top:40px;">
    <div class="services_menu" style="float:left;">
        <div class="services_menu_phis_header" id="jur_header">����������� ����</div>
        <div class="services_menu_phis_header not_active" id="phis_header">���������� ����</div>

        <ul class="services_menu_ul" id="phis_ul" style="display:none;">
            <li>
                <img src="images/phis/1n.png"/>
                <div id="services_menu_phis_1n">�������� �����</div>
            </li>
            <li>
                <img src="images/phis/2n.png"/>
                <div id="services_menu_phis_2n">��������� �����</div>
            </li>
            <li>
                <img src="images/phis/3n.png"/>
                <div id="services_menu_phis_3n">������ ��� ���</div>
            </li>
            <li>
                <img src="images/phis/4n.png"/>
                <div id="services_menu_phis_4n">�����������</div>
            </li>
            <li>
                <img src="images/phis/5n.png"/>
                <div id="services_menu_phis_5n">��������� ����</div>
            </li>
            <li>
                <img src="images/phis/6n.png"/>
                <div id="services_menu_phis_6n">��������� ������</div>
            </li>
            <li>
                <img src="images/phis/7n.png"/>
                <div id="services_menu_phis_7n">�������������� �����</div>
            </li>
        </ul>

        <ul class="services_menu_ul" id="jur_ul">
            <li>
                <img src="images/jur/1n.png"/>
                <div id="services_menu_jur_1n" style="text-decoration:underline; cursor:pointer;">�����������</div>
            </li>
            <li>
                <img src="images/jur/2n.png"/>
                <div id="services_menu_jur_2n" style="text-decoration:underline; cursor:pointer;">������ � �������������</div>
            </li>
            <li>
                <img src="images/jur/3n.png"/>
                <div id="services_menu_jur_3n">��������� �����</div>
            </li>
            <li>
                <img src="images/jur/4n.png"/>
                <div id="services_menu_jur_4n">������ ������� ���������</div>
            </li>
            <li>
                <img src="images/jur/5n.png"/>
                <div id="services_menu_jur_5n">���������� ��������������</div>
            </li>
            <li>
                <img src="images/jur/6n.png"/>
                <div id="services_menu_jur_6n">������ �� ����������</div>
            </li>
            <li>
                <img src="images/jur/7n.png"/>
                <div id="services_menu_jur_7n">���������� �����</div>
            </li>
            <li>
                <img src="images/jur/8n.png"/>
                <div id="services_menu_jur_8n">��������� �����</div>
            </li>
            <li>
                <img src="images/jur/9n.png"/>
                <div id="services_menu_jur_9n">��������</div>
            </li>
            <li>
                <img src="images/jur/10n.png"/>
                <div id="services_menu_jur_10n">��������������� �����</div>
            </li>
            <li>
                <img src="images/jur/11n.png"/>
                <div id="services_menu_jur_11n">���������� � ���������� �����</div>
            </li>
            <li>
                <img src="images/jur/12n.png"/>
                <div id="services_menu_jur_12n">�����-������� �������</div>
            </li>
            <li>
                <img src="images/jur/13n.png"/>
                <div id="services_menu_jur_13n">�������������� �������, ������ ������</div>
            </li>
        </ul>
    </div>
    <style>
        .service_wrapper { font-family:Verdana;}
        .service_wrapper h1 { color:#385d8a; font-size:26px;}
        .service_wrapper p { text-align:left; color:#606060; font-size:14px;}
        .service_wrapper table {font-size:14px; color:#606060; border-left:1px solid #385D8A;}
        .service_wrapper table th {border-right:1px solid #ffffff;padding:5px;
            text-align:left; color:#ffffff; font-size:14px; font-weight:normal; line-height:30px;
            background: rgb(56, 93, 138);
            background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJod�IgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
            background: -moz-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgb(56, 93, 138)), color-stop(99%, rgb(79, 129, 189)));
            background: -webkit-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: -o-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: -ms-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: linear-gradient(to bottom, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#200772', endColorstr='#207cca',GradientType=0 );
        }

        .service_wrapper table td {padding:5px;font-size:14px; color:#606060; border-right:1px solid #385D8A; border-bottom:1px solid #385D8A;}
    </style>
    <div style="width:600px; float:right; color:#000000;">

        <?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/bankrot.php");?>
        <?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/phis_nedviz.php");?>
    </div>
</div>

</div>
</div>

<div id="window_width"></div>


</body>
</html>

