    <h1>��������� �����</h1>
    <p>��������� ���� - ���� ����������. ������, ��� ��� ����������� �� ���������� ���������� ��� ����������� ���������� ������, ����� � ���������� ����������� ������� ��������, ��������� � �������� ���������� ����������. ��������� �������� ��������������� ���������� ������� ��������� ��������� ������� � ������������ � �������� ����������� ���������.</p>
    <p>� ���, ����� �� ������� ���� ����� � �����, ������������ �������� ���� �������� ��������������.</p>
    <h3><strong>�� �� ������ ��������� ����������� ������ ��� ���������� ������, ����������� �� ��������� ����� �����������:</strong></h3>
    <ol class="service_decimal_list">
        <li>������������� �����������;</li>
        <li>������ �����������;</li>
        <li>����������� ���������������;</li>
        <li>����������� ������������������� ������;</li>
        <li>���������� �����������.</li>
    </ol>
    <h3><strong>�� �� ������ ��������� ��������� ����������� ������ ��� ���������� ��������� ������:</strong></h3>
    <ul class="service_bird_list">
        <li>������ ������������;</li>
        <li>����������� ������ ����� � ���������� ����������������� ����������;</li>
        <li>����������� ���������� ����������, ��������������� ��������� ��������� (��������� �����������, ���������� ��������, ������� � ������� ���������� ���������� � �.�.);</li>
        <li>���������� �������� ������� �� �����;</li>
        <li>���������� �������������� ������ �� ��������� ���������;</li>
        <li>����������� ��������� ��������� ���������, ��������� � ����� � �����-������������� ��������������� ��������� ���������� �������;</li>
        <li>���������� � ������ ������� ���������, ����������, ��������� � ���� �������������� ����������;</li>
        <li>������������� � ������ ���� � �������� ��������� ������� � ����;</li>
        <li>�������������� ������������ (�������� ���������� ���������� � �������� ���� �������� �������������).</li>
    </ul>


