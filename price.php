<? include 'http://vzakone.spb.ru/header.php'?>

    <div id="top">
        <div class="price_wrapper" style="width:1000px; margin:0 auto;">
            <br /><br /><h1>������� ��������</h1><br />
            <style type="text/css">
                .price_table {width:100%;margin-left:20px; margin-right:20px;border-left:1px solid #385D8A; border-top:1px solid #385D8A;
                    font-size:16px;}
/*rgb 189 214 238*/
                .price_table th, .price_td_th {font-weight:bold; border-right:1px solid #ffffff; text-align:center; color:#ffffff; padding:10px;text-transform: uppercase;
                    background: rgb(56, 93, 138);
                    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJod�IgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
                    background: -moz-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
                    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgb(56, 93, 138)), color-stop(99%, rgb(79, 129, 189)));
                    background: -webkit-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
                    background: -o-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
                    background: -ms-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
                    background: linear-gradient(to bottom, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
                    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#200772', endColorstr='#207cca',GradientType=0 );
                }
                .price_table tr td {border-right:1px solid #385D8A; border-bottom:1px solid #385D8A;text-align:left; color:#606060; padding:15px;}
                .price_table tr td:nth-child(1) {font-weight:bold; color:#4F81BD; text-align:center;}
                .price_table tr th:nth-child(3), .price_td_th {border-right:none;}
                .price_wrapper h1 { color:#385d8a; font-size:26px; text-align:center;}
                .price_wrapper  {font-family:Palatino Linotype, MyPalatino, FontBookman, Arial;}
            </style>
            <table class="price_table" border=0 cellspacing=0 cellpadding=0 >
                <tr>
                    <th>�</th>
                    <th>������������ ������</th>
                    <th>��������� ������ (���.)</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>������ ������������ �� �������� ��������, �� ��������� ������� �������� � ��������������� ���������� ���������� </td>
                    <td>�� 500 �� 1000</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>������ ������������ �� �������� ��������, � ��������� �������� � ����������, ���� � ��������������� �����������</td>
                    <td>�� 1000 �� 3000</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>���������� ���������� � �������� ��������, ����������� ������������ ���������������� � �������� ��������</td>
                    <td>�� 3000 �� 5000</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>������������ � ������� � ������� (�� �����-����������)</td>
                    <td>1000 (������������� � ��������� ������������)</td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>����������� ������ (�������� ����������, ��������������, ����������� ���������, ���� ����������)</td>
                    <td>�� 1000 �� 5000</td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>����������� � ������ ������, ���������, ���������</td>
                    <td>�� 500 �� 2000</td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>����������� � ������ �������� ��������� � ���</td>
                    <td>�� 5000 �� 8000</td>
                </tr>
                <tr>
                    <td>8</td>
                    <td>����������� � ������ ������������� ������ � ���</td>
                    <td>�� 5000 �� 10000</td>
                </tr>
                <tr>
                    <td>9</td>
                    <td>����������� � ������ ������������ ������ � ���</td>
                    <td>�� 5000 �� 10000</td>
                </tr>
                <tr>
                    <td>10</td>
                    <td>����������� � ������ ��������� ������ � ��������� ��� ���������� ���������</td>
                    <td>�� 10000 �� 20000</td>
                </tr>
                <tr>
                    <td>11</td>
                    <td>����� (����������� �� �����-����������) ����������� �� ������, ����������, �������� ���������</td>
                    <td>�� 2000 �� 5000</td>
                </tr>
                <tr>
                    <td>12</td>
                    <td>����������� ��������� (�������, �����-�������, �����, ������, ������� ����� ����������, ����)</td>
                    <td>�� 3000 �� 10000</td>
                </tr>
                <tr>
                    <td>13</td>
                    <td>����������������� �� ������������ ���� � ���� ����� ���������� �� ����������� �������� ������ </td>
                    <td>�� 15000 �� 25000</td>
                </tr>
                <tr>
                    <td>14</td>
                    <td>����������������� �� ������������ ���� � ���� ����� ���������� �� ����������� �������� ������</td>
                    <td>�� 18000 �� 28000</td>
                </tr>
                <tr>
                    <td>15</td>
                    <td>����������������� �� ������������ ���� � ���� ����� ���������� �� ����������� ����������� ������</td>
                    <td>�� 20000 �� 30000</td>
                </tr>
                <tr>
                    <td>16</td>
                    <td>����������������� �� ������������ ���� � ���� ����� ���������� �� ����������� ��������� ������ </td>
                    <td>�� 25000 �� 40000</td>
                </tr>
                <tr>
                    <td>17</td>
                    <td>����������������� �� ������������ ���� � ���� ����� ���������� �� ������������ ����� ������� ����������, ������� ������� ��������, �������������� �������������� �������� ���������, ����������� ������������� ���������� �������������� ���������� </td>
                    <td>�� 40000 �� 100000
                        (�� ������� ���� + 10 % �� ���������� �����)
                    </td>
                </tr>
                <tr>
                    <td>18</td>
                    <td>����������������� �� ����������������� ���� � ���� ����� ����������</td>
                    <td>�� 20000 �� 60000</td>
                </tr>
                <tr>
                    <td>19</td>
                    <td>����������������� � ����������� ���� �� ������ �������������� ���������</td>
                    <td>�� 40000 (�� ������� ����
                        + 3% �� ���������� �����)
                    </td>
                </tr>
                <tr>
                    <td>20</td>
                    <td>����������������� � ����������� ���� �� ������ ���������������� ���������</td>
                    <td>�� 60000 (�� ������� ����
                        + 3% �� ���������� �����)
                    </td>
                </tr>
                <tr>
                    <td>21</td>
                    <td>������� ���������� ���� (� ����������� �� ��������� ����, ������ ���������� �������������, ����������� ������������� ����)</td>
                    <td>�� 50000 �� 300000</td>
                </tr>
                <tr>
                    <td colspan="3" class="price_td_th" style="color:#ffffff;">������� ������ ����������� ����� (�� ������ �������)</td>
                </tr>
                <tr>
                    <td>�.</td>
                    <td colspan="2">�������������� ������ ����������� �����.</td>
                </tr>
                <tr>
                    <td>�.</td>
                    <td colspan="2">������ ����������� ����� �� ������. ������ � ����� �������� �������� ������������ � �������������� �������.</td>
                </tr>

                <!--<style type="text/css">
                    #new_year_offer_text {color:#cd0a0a; font-style:italic; font-size:24px;}
                </style>
                <tr>
                    <td colspan="3" class="price_td_th" id="new_year_offer" style="color:#ffffff;letter-spacing: 12px;font-style: normal;font-size: 22px;">������������ �����������</td>
                </tr>
                <tr>
                    <td colspan="3" id="new_year_offer_text">������ �� 26.01.2015�. ������ �� ��� ������ �� ������������ 50%!</td>
                </tr>-->
            </table>

        </div>
    </div>
<? include 'http://vzakone.spb.ru/footer_menu.php'?>
<? include 'http://vzakone.spb.ru/footer.php'?>