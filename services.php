<? include 'http://vzakone.spb.ru/header.php'?>

    <style>
        .services_menu {
            /*border: 1px solid #200772;*/
            border-radius: 4px;
            width:320px;
            box-shadow: 0px 0px 7px #000;
        }

        .services_menu_phis_header {
            width:160px;
            height:50px;
            line-height:50px;
            float:left;
            text-align: center;
            font-family: Palatino Linotype, MyPalatino, FontBookman, Arial;
            font-size: 14px;
            color:#385D8A;
            font-weight:bold;
        }

        .services_menu_phis_header.not_active {
            background: rgb(56, 93, 138);
            background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJod�IgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
            background: -moz-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgb(56, 93, 138)), color-stop(99%, rgb(79, 129, 189)));
            background: -webkit-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: -o-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: -ms-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: linear-gradient(to bottom, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#200772', endColorstr='#207cca',GradientType=0 );
            color:#ffffff;
            opacity: 0.8;
            cursor:pointer;
        }

        .services_menu_ul {
            list-style-type: none;
            padding:0px;
            display:table;
        }

        .services_menu_ul li:last-child {
            margin-bottom:20px;
        }
        .services_menu_ul li {
            clear:both;
            margin-top:15px;
            margin-left:20px;
        }

        .services_menu_ul li img {
            float:left;
            height:45px;
        }

        .services_menu_ul li div {
            vertical-align:middle;
            font-family: Palatino Linotype, MyPalatino, FontBookman, Arial;
            font-size: 14px;
            padding-left:20px;
            text-align:left;
            width:200px;
            color:#385D8A;
            font-weight:bold;
            display:table-cell;
            height:45px;
        }
    </style>

<script type="text/javascript">

   $(document).ready(function(){
       $('#phis_header').click(function(){
           if($(this).hasClass('not_active')) {
               $(this).removeClass('not_active');
               $('#jur_header').addClass('not_active');
               $('#jur_ul').fadeOut(100);
               setTimeout(function() {$('#phis_ul').fadeIn(100);}, 100);

           }
           return false;
       });

       $('#jur_header').click(function(){
           if($(this).hasClass('not_active')) {
               $(this).removeClass('not_active');
               $('#phis_header').addClass('not_active');
               $('#phis_ul').fadeOut(100);
               setTimeout(function() {$('#jur_ul').fadeIn(100);}, 100);

           }
           return false;
       });

       /*$('#services_menu_jur_1n').click(function(){
           $('.service_wrapper').fadeOut(100);
           setTimeout(function() {$('#bankrot_text').fadeIn(100);}, 100);
           return false;
       });
       $('#services_menu_jur_2n').click(function(){
           $('.service_wrapper').fadeOut(100);
           setTimeout(function() {$('#nedv_text').fadeIn(100);}, 100);
           return false;
       })*/

       $('#phis_ul li div').click(function(){
           var phis_index = $(this).parent().index();
           $('.service_wrapper').removeClass('selected_serv');
           $('.phis_serv_list').eq(phis_index).addClass('selected_serv');
           var destination = $('#top').offset().top;
           $("body").animate({"scrollTop":destination},"slow");
           return false;
       });

       $('#phis_ul li img').click(function(){
           var phis_index = $(this).parent().index();
           $('.service_wrapper').removeClass('selected_serv');
           $('.phis_serv_list').eq(phis_index).addClass('selected_serv');
           var destination = $('#top').offset().top;
           $("body").animate({"scrollTop":destination},"slow");
           return false;
       });

       $('#jur_ul li div').click(function(){
           var jur_index = $(this).parent().index();
           $('.service_wrapper').removeClass('selected_serv');
           $('.jur_serv_list').eq(jur_index).addClass('selected_serv');
           var destination = $('#top').offset().top;
           $("body").animate({"scrollTop":destination},"slow");
           return false;
       });

       $('#jur_ul li img').click(function(){
           var jur_index = $(this).parent().index();
           $('.service_wrapper').removeClass('selected_serv');
           $('.jur_serv_list').eq(jur_index).addClass('selected_serv');
           var destination = $('#top').offset().top;
           $("body").animate({"scrollTop":destination},"slow");
           return false;
       });


   })

</script>
<style type="text/css">
    .phis_serv_list{display:none;}
    .phis_serv_list.selected_serv{display:block;}
    .jur_serv_list{display:none;}
    .jur_serv_list.selected_serv{display:block;}
    .services_menu_ul li div {text-decoration:underline; cursor:pointer;}
    .services_menu_ul li img {cursor:pointer;}
</style>

<?
$jur_tab = '';  $phis_tab=''; $jur_list=''; $phis_list='';
if(isset($_GET['article'])){
    $jur_tab = ''; $jur_list='style="display:none;"';
    $phis_tab='not_active'; $phis_list='';
    $list_number = $_GET['article'];?>
    <script type="text/javascript">
        $(document).ready(function(){
            var article_number = <?=$list_number?>;
            article_number = article_number - 1;
            $('.jur_serv_list').eq(article_number).addClass('selected_serv');
        })
    </script>
<?
} elseif(isset($_GET['phis_article'])){
    $jur_tab = 'not_active'; $jur_list='';
    $phis_tab=''; $phis_list='style="display:none;"';
    $phis_list_number = $_GET['phis_article'];?>
    <script type="text/javascript">
        $(document).ready(function(){
            var article_number = <?=$phis_list_number?>;
            article_number = article_number - 1;
            $('.phis_serv_list').eq(article_number).addClass('selected_serv');
        })
    </script>
<?
} else {
    $jur_tab = ''; $jur_list='style="display:none;"';
    $phis_tab='not_active'; $phis_list='';?>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.jur_serv_list').eq(0).addClass('selected_serv');
        })
    </script>
<?
}
?>


<div id="top" style="width:100%; display:table;">
<div style="width:1000px; margin:0 auto; margin-top:40px;">
    <div class="services_menu" style="float:left;">
        <div class="services_menu_phis_header <?=$jur_tab?>" id="jur_header">����������� ����</div>
        <div class="services_menu_phis_header <?=$phis_tab?>" id="phis_header">���������� ����</div>

        <ul class="services_menu_ul" id="phis_ul" <?=$jur_list?> >
            <li>
                <img src="images/phis2/nedv.png"/>
                <div id="services_menu_phis_1n">������ � �������������</div>
            </li>
            <li>
                <img src="images/phis2/nasl.png"/>
                <div id="services_menu_phis_2n">�������������� �����</div>
            </li>
            <li>
                <img src="images/phis2/land.png"/>
                <div id="services_menu_phis_3n">��������� �����</div>
            </li>
            <li>
                <img src="images/phis2/strach.png"/>
                <div id="services_menu_phis_4n">��������� �����</div>
            </li>
            <li>
                <img src="images/phis2/contract.png"/>
                <div id="services_menu_phis_5n">����� �� ���������</div>
            </li>
            <li>
                <img src="images/phis2/family.png"/>
                <div id="services_menu_phis_6n">�������� �����</div>
            </li>
            <li>
                <img src="images/phis2/flat.png"/>
                <div id="services_menu_phis_7n">�������� �����</div>
            </li>
            <li>
                <img src="images/phis2/labour.png"/>
                <div id="services_menu_phis_3n">�������� �����</div>
            </li>
            <li>
                <img src="images/phis2/rights.png"/>
                <div id="services_menu_phis_4n">������ ���� ������������</div>
            </li>
            <li>
                <img src="images/phis2/dtp.png"/>
                <div id="services_menu_phis_5n">������ ��� ���</div>
            </li>
            <li>
                <img src="images/phis2/argue.png"/>
                <div id="services_menu_phis_6n">����������� �������, �������� (�����������) ���, ����������� ���</div>
            </li>
            <li>
                <img src="images/phis2/crime.png"/>
                <div id="services_menu_phis_7n">������� ��������� ���</div>
            </li>
        </ul>

        <ul class="services_menu_ul" id="jur_ul" <?=$phis_list?>>
            <li>
                <img src="images/jur2/abonent.png"/>
                <div id="services_menu_jur_1n">����������� ������������</div>
            </li>
            <li>
                <img src="images/jur2/audit.png"/>
                <div id="services_menu_jur_2n">�������� �����</div>
            </li>
            <li>
                <img src="images/jur2/contract.png"/>
                <div id="services_menu_jur_3n">�����������, �������������, ����������</div>
            </li>
            <li>
                <img src="images/jur2/registr.png"/>
                <div id="services_menu_jur_4n">����� �� ���������</div>
            </li>
            <li>
                <img src="images/jur2/corporate.png"/>
                <div id="services_menu_jur_5n">������������� �����</div>
            </li>
            <li>
                <img src="images/jur2/tax.png"/>
                <div id="services_menu_jur_6n">��������� �����</div>
            </li>
            <li>
                <img src="images/jur2/tamoj.png"/>
                <div id="services_menu_jur_7n">���������� �����</div>
            </li>
            <li>
                <img src="images/jur2/argue.png"/>
                <div id="services_menu_jur_8n">����������� �������, �������� (�����������) ��� � ����������� ���</div>
            </li>
            <li>
                <img src="images/jur2/ispolnit.png"/>
                <div id="services_menu_jur_9n">�������������� ������������</div>
            </li>
            <li>
                <img src="images/jur2/nedv.png"/>
                <div id="services_menu_jur_10n">������ � �������������</div>
            </li>
        </ul>
    </div>
    <style type="text/css">


        .service_wrapper {font-family:Palatino Linotype, MyPalatino, FontBookman, Arial; font-size:16px;}
        .service_wrapper h1 { color:#385d8a; font-size:26px; text-align:center;}
        .service_wrapper h3 { color:#385d8a; }
        .service_wrapper p {color:#606060; text-align: justify; text-indent: 40px; margin:0px;}
        .service_wrapper table {font-size:14px; color:#606060; border-left:1px solid #385D8A;}

        /*����� ��� ������� � ������� �����*/
        .service_decimal_list li{list-style-type: decimal;color:#606060;}
        .service_bold {margin-bottom:15px; margin-top: 20px; font-weight: bold;}
        .service_quote {font-style:italic; width:100%; text-align: justify; text-indent: 40px;}
        .service_quote div{text-align:right; width:100%;}

        .service_cirillic_list li {position: relative; list-style-type: none; color:#606060;}
        .service_cirillic_list li:after {position: absolute; top: -1px; color: #385D8A; left: -25px;}
        .service_cirillic_list li:nth-child(1):after {content: 'a) ';}
        .service_cirillic_list li:nth-child(2):after {content: '�) ';}
        .service_cirillic_list li:nth-child(3):after {content: '�) ';}
        .service_cirillic_list li:nth-child(4):after {content: '�) ';}
        .service_cirillic_list li:nth-child(5):after {content: '�) ';}
        .service_cirillic_list li:nth-child(6):after {content: '�) ';}
        .service_cirillic_list li:nth-child(7):after {content: '�) ';}
        .service_cirillic_list li:nth-child(8):after {content: '�) ';}
        .service_cirillic_list li:nth-child(9):after {content: '�) ';}
        .service_cirillic_list li:nth-child(10):after {content: '�) ';}
        .service_cirillic_list li:nth-child(11):after {content: '�) ';}
        .service_cirillic_list li:nth-child(12):after {content: '�) ';}
        .service_cirillic_list li:nth-child(13):after {content: '�) ';}
        .service_cirillic_list li:nth-child(14):after {content: '�) ';}

        .service_bird_list li {position: relative; color:#606060;list-style: none;}
        .service_bird_list li:after {content: '\2714';position: absolute; top: -1px; color: #385D8A; left: -25px;}

        .service_line_list li {position: relative; color:#606060;list-style: none;}
        .service_line_list li:after {content: '\2012';position: absolute; top: -1px; color: #606060; left: -20px;}

        .service_circle_list li {color:#385D8A;}
        .service_circle_list li span {color:#606060;}


        /*������� - ��.���� - ����������� �����*/
        .services_audit_table {width:100%; border-left:1px solid #385D8A;border-top:1px solid #385D8A;}
        .services_audit_table th {font-weight:bold; border-right:1px solid #ffffff; text-align:center; color:#ffffff; padding:10px;
            background: rgb(56, 93, 138);
            background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJod�IgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
            background: -moz-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgb(56, 93, 138)), color-stop(99%, rgb(79, 129, 189)));
            background: -webkit-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: -o-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: -ms-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: linear-gradient(to bottom, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#200772', endColorstr='#207cca',GradientType=0 );
        }
        .services_audit_table tr th:nth-child(3) {border-right:none;}
        .services_audit_table tr td {border-right:1px solid #385D8A; border-bottom:1px solid #385D8A;text-align:center; padding:10px;}
        .services_audit_table tr td:nth-child(1) {font-weight:bold;}
        .services_audit_table tr td:nth-child(2) {text-align:left;}
        .services_audit_table tr td:nth-child(3) {font-weight:bold; font-style:italic;}

        /*������� - ��.���� - �����������*/
        .services_registr_table {width:100%; border-left:1px solid #385D8A;border-top:1px solid #385D8A;}
        .services_registr_table th {font-weight:bold; border-right:1px solid #ffffff; text-align:center; color:#ffffff; padding:10px;
            background: rgb(56, 93, 138);
            background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJod�IgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
            background: -moz-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgb(56, 93, 138)), color-stop(99%, rgb(79, 129, 189)));
            background: -webkit-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: -o-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: -ms-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            background: linear-gradient(to bottom, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#200772', endColorstr='#207cca',GradientType=0 );
        }
        .services_registr_table tr:nth-child(2) td{border-top:1px solid #4F81BD;}
        .services_registr_table tr th:nth-child(3) {border-right:none;}
        .services_registr_table tr td {border-right:1px solid #385D8A; border-bottom:1px solid #385D8A;text-align:center; padding:10px;}
        .services_registr_table tr td:nth-child(3) {text-align: left; padding-left:20px;}
        .service_table_headers {font-style:italic; font-size:24px;}



    </style>
    <div style="width:600px; float:right; color:#000000;">
        <div class="service_wrapper phis_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/phis_nedviz.php");?></div>
        <div class="service_wrapper phis_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/phis_nasl.php");?></div>
        <div class="service_wrapper phis_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/phis_land.php");?></div>
        <div class="service_wrapper phis_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/phis_assure.php");?></div>
        <div class="service_wrapper phis_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/phis_contract.php");?></div>
        <div class="service_wrapper phis_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/phis_family.php");?></div>
        <div class="service_wrapper phis_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/phis_flat.php");?></div>
        <div class="service_wrapper phis_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/phis_labour.php");?></div>
        <div class="service_wrapper phis_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/phis_rights.php");?></div>
        <div class="service_wrapper phis_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/phis_dtp.php");?></div>
        <div class="service_wrapper phis_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/phis_argue.php");?></div>
        <div class="service_wrapper phis_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/phis_crime.php");?></div>

        <div class="service_wrapper jur_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/jur_abonent.php");?></div>
        <div class="service_wrapper jur_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/jur_audit.php");?></div>
        <div class="service_wrapper jur_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/jur_registr.php");?></div>
        <div class="service_wrapper jur_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/jur_contract.php");?></div>
        <div class="service_wrapper jur_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/jur_corporate.php");?></div>
        <div class="service_wrapper jur_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/jur_tax.php");?></div>
        <div class="service_wrapper jur_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/jur_tamoj.php");?></div>
        <div class="service_wrapper jur_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/jur_argue.php");?></div>
        <div class="service_wrapper jur_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/jur_product.php");?></div>
        <div class="service_wrapper jur_serv_list"><?include_once($_SERVER["DOCUMENT_ROOT"] . "/service_description/jur_proper.php");?></div>
    </div>
</div>
</div>
<? include 'http://vzakone.spb.ru/footer_menu.php'?>
<? include 'http://vzakone.spb.ru/footer.php'?>
