<style type="text/css">
    #content_wrapper_footer {margin-top: 80px; padding-top: 30px; padding-bottom: 10px; width: 100%;
        background: #385D8A;
        background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJod�IgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
        background: -moz-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #385D8A), color-stop(99%, #4F81BD));
        background: -webkit-linear-gradient(top, #385D8A 0%, #4F81BD 99%);
        background: -o-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
        background: -ms-linear-gradient(top, rgb(56, 93, 138) 0%, rgb(79, 129, 189) 99%);
        background: linear-gradient(to bottom, #385D8A 0%, #4F81BD 99%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#200772', endColorstr='#207cca',GradientType=0 );
    }
    .footer_container {width: 100%;height: 400px;}
    .footer_menu_item {float:left; }
    .footer_menu_item div {text-transform: uppercase; color:#FFF;font-style: normal; font-family: Verdana; font-size: 15px; font-weight:bold; margin-top:20px;}
    .footer_menu_item ul {padding:0;}
    .footer_menu_item ul li{width:300px; padding:0; margin:6px; margin-left:0;}
    .footer_menu_item ul li a{color:#fff; padding:0; margin:3px;font-style: italic; font-family: Verdana; font-size: 13px;}
    .footer_menu_item_right {float:right; height:390px; width:270px; position:relative;}
    .footer_menu_item_right_title {text-transform: uppercase; color:#FFF;font-style: normal; font-family: Verdana; font-size: 15px; font-weight:bold; margin-top:20px;}
    .footer_menu_item_right_text {color:#fff; padding:0; margin-top:8px;font-style: italic; font-family: Verdana; font-size: 13px;}
    .footer_menu_item_right_text a{color:#fff;}

</style>

<div id="content_wrapper_footer">
    <div class="footer_container">
        <div style="width:1000px; margin:0 auto; position:relative;">
            <div class="footer_menu_item">
                <div>������ ��� ���������� ���</div>
                <ul>
                    <li><a href="/services.php?phis_article=1#top">������ � �������������</a></li>
                    <li><a href="/services.php?phis_article=2#top">�������������� �����</a></li>
                    <li><a href="/services.php?phis_article=3#top">��������� �����</a></li>
                    <li><a href="/services.php?phis_article=4#top">��������� �����</a></li>
                    <li><a href="/services.php?phis_article=5#top">����� �� ���������</a></li>
                    <li><a href="/services.php?phis_article=6#top">�������� �����</a></li>
                    <li><a href="/services.php?phis_article=7#top">�������� �����</a></li>
                    <li><a href="/services.php?phis_article=8#top">�������� �����</a></li>
                    <li><a href="/services.php?phis_article=9#top">������ ���� ������������</a></li>
                    <li><a href="/services.php?phis_article=10#top">������ ��� ���</a></li>
                    <li><a href="/services.php?phis_article=11#top">����������� �������, �������� (�����������) ���, ����������� ���</a></li>
                    <li><a href="/services.php?phis_article=12#top">������� ��������� ���</a></li>
                </ul>
            </div>
            <div class="footer_menu_item" style="margin-left: 60px;">
                <div>������ ��� ����������� ���</div>
                <ul>
                    <li><a href="/services.php?article=1#top">����������� ������������</a></li>
                    <li><a href="/services.php?article=2#top">�������� �����</a></li>
                    <li><a href="/services.php?article=3#top">�����������, �������������, ����������</a></li>
                    <li><a href="/services.php?article=4#top">����� �� ���������</a></li>
                    <li><a href="/services.php?article=5#top">������������� �����</a></li>
                    <li><a href="/services.php?article=6#top">��������� �����</a></li>
                    <li><a href="/services.php?article=7#top">���������� �����</a></li>
                    <li><a href="/services.php?article=8#top">����������� �������, �������� (�����������) ��� � ����������� ���</a></li>
                    <li><a href="/services.php?article=9#top">�������������� ������������</a></li>
                    <li><a href="/services.php?article=10#top">������ � �������������</a></li>
                </ul>
            </div>
            <div class="footer_menu_item_right">
                <div style="float:right;">

                    <div class="footer_menu_item_right_title">�������</div>
                    <div class="footer_menu_item_right_text">8-981-728-83-83<br />8-981-728-85-85</div>
                    <div class="footer_menu_item_right_title">E-mail</div>
                    <div class="footer_menu_item_right_text"><a href="mailto:vzakone.spb@mail.ru">vzakone.spb@mail.ru</a></div>
                    <div class="footer_menu_item_right_title">�����</div>
                    <div class="footer_menu_item_right_text">�. �����-���������, <br />��. ������, �.87, �.9<br />(3-� ����)<br /><a href="/map.php#top">�� �����</a></div>
                    <div class="footer_menu_item_right_text" style="margin-top:30px;"><a href="/map.php#callback_container">����� �������� �����</a></div>

                </div>
                <div class="footer_menu_item_right_text" style="position:absolute; right:0px; bottom:10px;">&copy; �� �� ������ 2014&ndash;2015</div>
            </div>
        </div>
    </div>
</div>




