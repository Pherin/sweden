function captcha_ajax() {
    $.get(
        "http://vzakone.spb.ru/mail/check_captcha.php?text="+$('#user_capcha').val(),
        onAjaxSuccess
    );

    function onAjaxSuccess(data){
        if (data == 'no') {

            $('#capcha-image').addClass('invalid');
            $('#capcha-image').after('<div class="error">���� ������� ������� ����� � ��������</div>');
            $('#capcha-image').nextAll(".error").delay(2000).fadeOut("slow");
            setTimeout(function () {
                $("#user_capcha").next(".error").remove();
            }, 2600);
            $('#capcha-image').attr('src','captcha.php?'+Math.round(Math.random()*100000));
        } else {
            var msg = $("#form").serialize();
            $.ajax({
                type: "POST",
                url: "http://vzakone.spb.ru/mail/letter_mailing.php",
                data: msg,
                success: function () {
                    $(".all_form_wrapper").hide();
                    $("#results").html('���� ��������� ������� ����������.');
                },
                error: function (xhr, str) {
                    alert("�������� ������!");
                }
            });
        }
    }
}

jQuery.fn.notExists = function () { //�������� �� ������������� ��������
    return $(this).length == 0;
}

$(document).ready(function () { //��������� �����
    $(".send").validation(
        $(".name").validate({
            test: "blank letters",
            invalid: function () {
                if ($(this).nextAll(".error").notExists()) {
                    $(this).after('<div class="error">������� ���������� ���</div>');
                    $(this).nextAll(".error").delay(2000).fadeOut("slow");
                    setTimeout(function () {
                        $(".name").next(".error").remove();
                    }, 2600);
                }
            },
            valid: function () {
                $(this).nextAll(".error").remove();
            }
        }),
        $(".email").validate({
            test: "blank email",
            invalid: function () {
                if ($(this).nextAll(".error").notExists()) {
                    $(this).after('<div class="error">������� ���������� e-mail</div>');
                    $(this).nextAll(".error").delay(2000).fadeOut("slow");
                    setTimeout(function () {
                        $(".email").next(".error").remove();
                    }, 2600);
                }
            },
            valid: function () {
                $(this).nextAll(".error").remove();
            }
        }),
        $(".subject").validate({
            test: "blank",
            invalid: function () {
                if ($(this).nextAll(".error").notExists()) {
                    $(this).after('<div class="error">������� ����</div>');
                    $(this).nextAll(".error").delay(2000).fadeOut("slow");
                    setTimeout(function () {
                        $(".subject").next(".error").remove();
                    }, 2600);
                }
            },
            valid: function () {
                $(this).nextAll(".error").remove();
            }
        }),
        $(".message").validate({
            test: "blank",
            invalid: function () {
                if ($(this).nextAll(".error").notExists()) {
                    $(this).after('<div class="error">������� ���������</div>');
                    $(this).nextAll(".error").delay(2000).fadeOut("slow");
                    setTimeout(function () {
                        $(".message").next(".error").remove();
                    }, 2600);
                }
            },
            valid: function () {
                $(this).nextAll(".error").remove();
            }
        }));
});